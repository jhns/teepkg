# TeePKG

Welcome to TeePKG, a package management script that calls the local <br>
package manager to manage packages on your Linux or BSD operating system.

## Features

- Install packages
- Remove packages
- Search for packages
- Upgrade all packages
- Clean the system
- Show package informations

## Package Managers

- apt (Debian, Ubuntu)
- dnf (Fedora, RHEL, AlmaLinux and other RHEL-based distributions)
- yum (old versions of RHEL and RHEL-based distributions)
- pacman (Arch Linux)
- xbps (Void Linux)
- zypper (openSUSE, SLES/SLED)
- eopkg (Solus)
- pkg (FreeBSD)
- pkg_add (OpenBSD)
- emerge (Gentoo)
- nix (NixOS)

## Instructions

Linux/OpenBSD: `make install` <br>
FreeBSD: `make bsd`

Uninstall: `make uninstall`

## Current problems

- There is currently no command for cleaning packages with nix.
- There is currently no command for showing package informations with emerge.

## License

TeePKG is licensed under the 3-Clause BSD License.
